<link rel="stylesheet" type="text/css" href="css.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.qrcode/1.0/jquery.qrcode.min.js"></script>
<div class="qr-code-generator">

<input type="text" class="qr-url" placeholder="URL or Text">
<input type="number" class="qr-size" value="128" min="20" max="500">

<button class="generate-qr-code" onclick="genarate_qr();">Generate</button>

<br>

<div id="qrcode"></div>

</div>
<script>
function genarate_qr()
{

// Clear Previous QR Code
$('#qrcode').empty();

// Set Size to Match User Input
$('#qrcode').css({
'width' : $('.qr-size').val(),
'height' : $('.qr-size').val()
})

// Generate and Output QR Code
$('#qrcode').qrcode({width: $('.qr-size').val(),height:$('.qr-size').val(),text: $('.qr-url').val()});

}
	</script>